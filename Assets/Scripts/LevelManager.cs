﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] int blockCount = 0;

    SceneLoader sceneLoader;

    private void Start()
    {
        //blockCount = 0;
        sceneLoader = FindObjectOfType<SceneLoader>();
        Time.timeScale = 1f;
    }

    public void AddBlockCount()
    {
        blockCount++;
    }

    public void BlockDestroyed()
    {
        blockCount--;
        if(blockCount <= 0)
        {
            sceneLoader.LoadNextSceneWithDelay();
            Time.timeScale = 0.3f;
        }
    }
}
