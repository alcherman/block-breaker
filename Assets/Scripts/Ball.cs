﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] Pad pad;

    Vector3 ballOffset;
    bool hasStarted = false;
    Rigidbody2D rigibody;
    AudioSource audioSource;


    public void RestartBall()
    {
        hasStarted = false;
    }
    public void LaunchBall()
    {
        float randomX = Random.Range(-1f, 1f);
        Vector2 newSpeed = new Vector2(randomX, 10);
        rigibody.velocity = newSpeed.normalized * speed;
        //rigibody.AddForce(new Vector2(2, 10));
        hasStarted = true;
    }

    void Awake()
    {
        rigibody = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //define ball offset on game start
        ballOffset = transform.position - pad.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(rigibody.velocity.magnitude);
        if (!hasStarted)
        {
            LockBallToPad();
            CheckLaunchBall();
        }
    }

    private void LockBallToPad()
    {
        //apply ball offset
        transform.position = pad.transform.position + ballOffset;
    }

    private void CheckLaunchBall()
    {
        if (Input.GetMouseButtonDown(0))
        {
            LaunchBall();
        }
    }


    private void OnDrawGizmos()
    {
        if(Application.isPlaying)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + (Vector3)rigibody.velocity);
            //Debug.Log(rigibody.velocity.magnitude);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        audioSource.Play();
        if (collision.gameObject.tag == "Pad")
        {
            CorrectVelocity();
        }
    }

    private void CorrectVelocity()
    {
        BoxCollider2D padCollider = pad.GetComponent<BoxCollider2D>();
        float padWidth = (padCollider.size.x * pad.transform.localScale.x)/ 2;

        float padPositionX = pad.transform.position.x;
        float ballPositionX = transform.position.x;

        float distance = ballPositionX - padPositionX;
        float percent = distance / padWidth;

        float newXSpeed = percent;
        Vector2 newSpeed = new Vector2(newXSpeed, 1);
        rigibody.velocity = newSpeed.normalized * speed;

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Vector2 newSpeed = rigibody.velocity;
        rigibody.velocity = newSpeed.normalized * speed;
    }
}
