﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] int totalScore = 0;
    [SerializeField] int lives = 3;

    [Header("UI Elements")]
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] GameObject livePrefab;
    [SerializeField] Transform livesContainer;


    SceneLoader sceneLoader;
    AudioSource audioSource;

    public void AddScore(int scoreToAdd)
    {
        totalScore += scoreToAdd;
        scoreText.text = totalScore.ToString();
    }

    public void LoseLive()
    {
        lives--;
        if (lives <= 0)
        {
            sceneLoader.LoadFirstScene();
        }
        else
        {
            Transform firstChild = livesContainer.GetChild(0);
            Destroy(firstChild.gameObject);
        }
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    void Awake()
    {
        if (FindObjectsOfType<GameManager>().Length > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        audioSource = GetComponent<AudioSource>();
        sceneLoader = FindObjectOfType<SceneLoader>();
    }

    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = totalScore.ToString();
        DontDestroyOnLoad(gameObject);

        for(int i = 0; i < lives; i++)
        {
            Instantiate(livePrefab, livesContainer);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
