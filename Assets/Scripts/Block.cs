﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] int score = 10;
    [SerializeField] Sprite[] sprites;
    [SerializeField] GameObject pickup;
    [SerializeField] bool exploding;
    [SerializeField] AudioClip destroySound;
    [Range(0, 10)][SerializeField] float explodeRadius = 1f;


    int health = 1;
    int hits = 0;

    SpriteRenderer spriteRenderer;
    GameManager gameManager;
    LevelManager levelManager;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        health = sprites.Length;
        gameManager = FindObjectOfType<GameManager>();
        levelManager = FindObjectOfType<LevelManager>();
        levelManager.AddBlockCount();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        hits++;
        if (hits >= health)
        {
            DestroyBlock();
        }
        else
        {
            if (sprites[hits] == null)
            {
                Debug.LogError(gameObject.name + " - Sprite element not set: Element " + hits);
            }
            else
            {
                spriteRenderer.sprite = sprites[hits];
            }
        }
    }

    public void DestroyBlock()
    {
        gameManager.AddScore(score);
        levelManager.BlockDestroyed();
        Destroy(gameObject);

        gameManager.PlaySound(destroySound);

        if (pickup != null)
        {
            Instantiate(pickup, transform.position, Quaternion.identity);
        }

        if (exploding)
        {
            Collider2D[] objects = Physics2D.OverlapCircleAll(transform.position, explodeRadius, LayerMask.GetMask("Block"));

            //for (int i = 0; i < objects.Length; i++)
            //{
            //    Collider2D block = objects[i];
            //    Destroy(block.gameObject);
            //}

            foreach(Collider2D block in objects)
            {
                Block blockToDestory = block.gameObject.GetComponent<Block>();
                blockToDestory.DestroyBlock();
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (exploding)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, explodeRadius);
        }
    }
}
