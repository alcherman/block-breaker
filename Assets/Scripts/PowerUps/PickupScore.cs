﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScore : MonoBehaviour
{
    [SerializeField] int score = 100;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //   if (collision.CompareTag("Pad"))
        if (collision.tag == "Pad")
        {
            //Debug.Log("Trigger pad");
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.AddScore(score);
            Destroy(gameObject);
        }
    }
}
