﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseGame : MonoBehaviour
{
    GameManager gameManager;
    Ball ball;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        ball = FindObjectOfType<Ball>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Invoke("Restart", 0.3f);
    }

    void Restart()
    {
        gameManager.LoseLive();
        ball.RestartBall();
    }
}
